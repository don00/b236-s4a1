
package com.zuitt.activity;

public class Main {
    public static void main(String [] args){

       Phonebook phonebook = new Phonebook(); 
       
       Contact contact1 = new Contact();
       Contact contact2 = new Contact();
       
       contact1.setName("John");
       contact1.setContactNumber("+639152468596");
       contact1.setAddress("Quezon City");
       
       phonebook.addContact(contact1);
       phonebook.addContact(contact2);
       
       printPhonebook(phonebook);
       
       Phonebook phonebook2 = new Phonebook(phonebook); 
       
       printPhonebook(phonebook2);
    }
    
    public static void printPhonebook(Phonebook phonebook){
       if(phonebook.getContacts().isEmpty()){
           System.out.println("Phonebook is currently empty");
       } else {
           phonebook.getContacts().forEach(contact -> {
               System.out.println("------------------");
               System.out.println(contact.getName());
               System.out.println("------------------");
               System.out.println(contact.getName() + " has the follwoing registered number: ");
               System.out.println(contact.getContactNumber());
               System.out.println(contact.getName() + " has the follwoing registered address: ");
               System.out.println(contact.getAddress());
           });
       }    
    }
    
    
}
