
package com.zuitt.activity;
import java.util.ArrayList;

public class Phonebook {
    private ArrayList<Contact> contacts;
    
    public Phonebook(){
        this.contacts  = new ArrayList<Contact>();
    }
    
    // constructor for creating a phonebook using another existing phonebook object
    public Phonebook(Phonebook phonebook){
        this.contacts = phonebook.getContacts();
    }
    
    // constructor for initializing a phonebook with 1 Contact object supplied as parameter
    public Phonebook(Contact contact){
        this.contacts = new ArrayList<Contact>();
        this.contacts.add(contact);
    }
    
    public void addContact(Contact contact){
        this.contacts.add(contact);
    }
    
    // setter using a phonebook object as parameter
    public void addContact(Phonebook phonebook){
        this.contacts.addAll(phonebook.getContacts());
    }
    
    public ArrayList<Contact> getContacts(){
        return this.contacts;
    }
    
    
   
}
