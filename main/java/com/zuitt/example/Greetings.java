
package com.zuitt.example;

/**
 *
 * @author Don_2
 */
public interface Greetings {
    
    public void morningGreet();
    public void holidayGreet();
}
