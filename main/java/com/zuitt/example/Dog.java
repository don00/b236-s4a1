
package com.zuitt.example;


public class Dog extends Animal{
    private String breed;
    
    public Dog(){
        super();
        this.breed = "Chihuaha";
    }
    
    public Dog(String name, String color, String breed){
        super(name,color);
        this.breed = breed; 
    }
    
    public String getBreed(){
        return this.breed;
    }
    
    public void setBreed(String breed){
        this.breed = breed;
    }
}
