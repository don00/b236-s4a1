package com.zuitt.example;

public class Animal {
    // properties
    private String name;
    private String color;
    
    
    public Animal(){}
    
    public Animal(String name, String color){
        this.name = name;
        this.color = color;
    }
    
    public String getName(){
        return this.name;
    }
    
    public String getColor(){
        return this.color;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public void setColor(String color){
        this.color = color;
    }
    
    public void speak(){
        System.out.println("Woof woof!");
    }
    
    public void call(){
        System.out.println("HI my name is " + this.name);
    }
    
    
}
