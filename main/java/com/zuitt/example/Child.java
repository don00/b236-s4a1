/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.zuitt.example;

/**
 *
 * @author Don_2
 */
public class Child extends Parent{
    
    @Override
    public void speak(){
        System.out.println("I am a child");
    }
}
